import fetch from 'node-fetch'

let handler = async (m, { conn, usedPrefix, args, command, text }) => {
if (!args[0]) throw `Linknya Mana?`
  let res = await fetch(`https://api.zeltoria.my.id/api/download/tiktok?url=${args[0]}&apikey=${global.zeltoria}`)
  let x = await res.json()
  let cap = `
*Nama:* ${x.author.nickname}
*Negara:* ${x.region}
*Durasi:* ${x.duration}
*Komentar:* ${x.statistics.comment_count}
*Deskripsi:*
${x.desc}
`
  conn.sendFile(m.chat, x.download.nowm, 'tiktok.mp4', cap, m)
}
handler.help = ['tiktok'].map(v => v + ' <link>')
handler.tags = ['downloader']
handler.command = /^(tiktok|tt|ttdl|tiktokdl)$/i
handler.limit = true

export default handler
