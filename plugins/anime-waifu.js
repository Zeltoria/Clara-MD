import fetch from 'node-fetch'

let handler = async (m, { conn, command }) => {
	let waifu = `https://api.ibeng.tech/api/wallpaper/waifu?apikey=${global.ibeng}`
	conn.sendMessage(m.chat, { image: { url: waifu }, caption: 'Awpokawpaok Gepeng' }, m)
}
handler.command = /^(waifu)$/i
handler.tags = ['anime']
handler.help = ['waifu']
handler.limit = true
export default handler
